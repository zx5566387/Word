package word.Entity;

import javax.persistence.*;
import lombok.Data;

/*
    跟问题相关的实体类
 */

@Data
@Entity
@Table(name = "problem")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "problemId")
    private int quesId;//问题ID
    @Column(name = "questionnaireId")
    private int questionnaireId;//所在的问卷ID
    @Column(name = "serialNum")
    private String serialNum;//序号,如:*A1*
    @Column(name = "problemName")
    private String quesName;//字段名称,如:有限股份公司
    @Column(name = "answer")
    private String quesAns;//字段内容
    @Column(name = "state")
    private int state;//状态

    public Question() {}
    /*
        添加问题
     */
    public Question(String serialNum,String quesName,String quesAns)
    {
        this.serialNum=serialNum;
        this.quesName=quesName;
        this.quesAns=quesAns;
    }
    public void SetQuestion(String serialNum,String quesName,String quesAns)
    {
        this.serialNum=serialNum;
        this.quesName=quesName;
        this.quesAns=quesAns;
    }
}
