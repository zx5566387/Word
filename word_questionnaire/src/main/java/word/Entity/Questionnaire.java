package word.Entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/*
    跟问卷相关的实体类
 */

@Data
@Entity
@Table(name = "questionnaire")
public class Questionnaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "questionnaireId")
    private int questionnaireId;//问卷ID
    @Column(name = "projectId")
    private int projectId;//项目ID
    @Column(name = "questionnaireTitle")
    private String title;//问卷标题
    @Transient
    private List<Question> questionList;//问卷表下的问题
    /*
        初始化
     */
    public void SetQuestionnaire(int projectId,String title)
    {
        if(questionList==null)questionList=new ArrayList<Question>();
        this.projectId=projectId;
        this.title=title;
    }
    /*
        向问卷添加问题
     */
    public void SetQuestion(Question ques)//添加一个问题
    {
        try {
            questionList.add(ques);
        }catch (NullPointerException ne)
        {
            questionList=new ArrayList<Question>();
            questionList.add(ques);
            //System.out.println("quetionList为空,新建");
        }catch (Exception e)
        {//System.out.println("其他错误!");
        }
    }
    public void SetQuestion(List<Question> ques)//添加或导入若干个问题
    {
        if(questionList==null)
        {
            questionList=ques;
            return;
        }
        for (Question q:ques
             ) {
            questionList.add(q);
        }
    }
    /*
        输出测试
     */
    @Override
    public String toString() {
        return "问卷信息{" +
                "问卷ID=" + questionnaireId +
                ", 项目ID=" + projectId +
                ", 问卷名='" + title + '\'' +
                ", 问题列表=" + questionList +
                '}';
    }
}