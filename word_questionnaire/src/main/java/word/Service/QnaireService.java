package word.Service;

import org.springframework.web.multipart.MultipartFile;
import word.Entity.Questionnaire;

import java.util.List;

public interface QnaireService {
    //创建一张空白问卷(读入项目ID和问卷名称,自动生成一张空白问卷,其中问卷Id由数据库自动生成)
    void CreatedQuestionnaire(int projectId, String questionnaireName);
    //读取所有问卷
    List<Questionnaire> LoadAllQuestionnaire();
    //读取指定的问卷(传入问卷ID,从 问题数据表problem 中获取该问卷对应的问题数据)
    Questionnaire LoadQuestionnaire(int questionnaireId);
    //修改问卷名称(通过问卷Id,用 变量newQuestionnaireTitle 对 问卷表questionnaire 中该问卷的标题 questionnaireTitle 进行覆盖操作)
    void SaveNewQuestionnaireTitle(int questionnaireId, String newQuestionnaireTitle);
    //删除问卷中的问题(通过问题的Id定位到问题并进行删除)
    void DeleteProblem(int[] problemId);//批量删除问题
    //删除问卷(通过问卷Id进行定位,对 问题表problem 和 问卷表questionnaire 中对应的字段进行删除操作)
    void DropQuestionnaire(int questionnaireId);
    //更新v1:添加新方法:通过项目Id读取问卷所有信息
    List<Questionnaire> LoadQuestionnairesByProjectId(int projectId);
    //更新v1:添加新方法:读入一张Excel表生成问卷
    int BuildQuestionnaireByExcel(int projectId, String filePath);

    //更新v2:修改原有方法:更新问卷(通过问卷Id对 问题数据表problem 中对应的数据字段进行覆盖操作)
    void SaveNewQuestionnaire(Questionnaire questionnaire);
    //更新v2:添加新方法:读入Excel表生成问卷
    int BuildQuestionnaireByExcel(int projectId, MultipartFile multipartFile);
}
