package word.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import word.Entity.Questionnaire;
import word.Service.QnaireService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/Questionnaire")
@Api(tags = "工具：问卷模块")
public class QnaireController {
    //问卷相关服务
    @Autowired
    QnaireService qnaireService;
    //创建新问卷(传入项目ID:int projectId,问卷标题:String title,创建成功返回1,否则返回-1)
    @ApiOperation(value = "创建新问卷")
    @RequestMapping(value = "/Questionnaire/Created",method = RequestMethod.POST)
    @ResponseBody
    public int CreateNewQuestionnaire(@RequestBody Map<String,Object> data)
    {
        try
        {
            int id=Integer.parseInt(data.get("projectId").toString());
            String title=data.get("title").toString();

            qnaireService.CreatedQuestionnaire(id,title);
            return 1;
        }catch (Exception e)
        {
            return -1;
        }
    }
    //读取指定问卷(传入问卷Id:int questionnaireId,成功则读取该问卷信息以及问卷下的问题数据,失败返回null)
    @ApiOperation(value = "读取指定问卷")
    @RequestMapping(value = "/Questionnaire/LoadDetail",method = RequestMethod.GET)
    @ResponseBody
    public Questionnaire LoadQuestionnaire(int questionnaireId)
    {
        try
        {
            Questionnaire data=qnaireService.LoadQuestionnaire(questionnaireId);
            return data;
        }catch (Exception e)
        {
            return null;
        }
    }
    //查看系统上所有问卷(管理员权限尚未添加)
    @ApiOperation(value = "查看系统上所有问卷")
    @RequestMapping(value = "/Questionnaire/LoadList",method = RequestMethod.GET)
    @ResponseBody
    public List<Questionnaire> LoadQuestionnaireList()
    {
        return qnaireService.LoadAllQuestionnaire();
    }
    //修改问卷标题(传入问卷id:int questionnaireId,问卷标题:String title)
    @ApiOperation(value = "修改问卷标题")
    @RequestMapping(value = "/Questionnaire/UpdateTitle",method = RequestMethod.POST)
    @ResponseBody
    public int UpdateQuestionnaireTitle(@RequestBody Map<String,Object> data)
    {
        try
        {
            int id=Integer.parseInt(data.get("questionnaireId").toString());
            String title=data.get("title").toString();
            qnaireService.SaveNewQuestionnaireTitle(id, title);
            return 1;
        }catch (Exception e)
        {
            return -1;
        }
    }
    //移除问题(传入问题一组问题id:int[] questionId)
    @ApiOperation(value = "移除问题")
    @RequestMapping(value = "/Questionnaire/RemoveQuestion",method = RequestMethod.POST)
    @ResponseBody
    public int RemoveProblem(@RequestBody int[]id)
    {
        try
        {
            qnaireService.DeleteProblem(id);
            return 1;
        }catch (Exception e)
        {
            return -1;
        }
    }
    //删除问卷(传入问卷id:int questionnaireId)
    @ApiOperation(value = "删除问卷")
    @RequestMapping(value = "/Questionnaire/DropQuestionnaire",method = RequestMethod.POST)
    @ResponseBody
    public int DropQuestionnaire(@RequestBody int questionnaireId)
    {
        try
        {
            qnaireService.DropQuestionnaire(questionnaireId);
            return 1;
        }catch (Exception e)
        {
            return -1;
        }
    }
    //更新v1:添加新接口:根据项目id读取旗下所有问卷信息(传入项目id:projectId)
    @ApiOperation(value = "根据项目id读取旗下所有问卷信息")
    @RequestMapping(value = "/Questionnaire/LoadProject/{projectId}",method = RequestMethod.GET)
    @ResponseBody
    public List<Questionnaire> LoadProject(@PathVariable("projectId") int id)
    {
        try {
            return qnaireService.LoadQuestionnairesByProjectId(id);
        }catch (Exception e)
        {
            return null;
        }
    }

    //更新v2:修改原有接口:更新问卷(传入问卷数据)
    @ApiOperation(value = "更新问卷")
    @RequestMapping(value = "/Questionnaire/UpdateQuestionnaire",method = RequestMethod.POST)
    @ResponseBody
    public int UpdateQuestionnaire(@RequestBody Questionnaire data)
    {
        //System.out.println(data.toString());
        try
        {
            //通过问卷id和问题数据数组保存问卷
            qnaireService.SaveNewQuestionnaire(data);
            return 1;
        }catch (Exception e)
        {
            return -1;
        }
    }
    //更新v2:修改原有接口:读入项目id和变量表,生成问题
    @ApiOperation(value = "读入项目id和变量表,生成问题")
    @RequestMapping(value = "/Questionnaire/BuildQuestionnaire",method = RequestMethod.POST)
    @ResponseBody
    public int BuildQuestionnaire(@RequestParam(value="projectId")int projectId,@RequestParam(value="multipartFile") MultipartFile file)
    {
        int result = qnaireService.BuildQuestionnaireByExcel(projectId,file);
        return result;
    }

}