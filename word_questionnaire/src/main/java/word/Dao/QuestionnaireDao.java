package word.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import word.Entity.Questionnaire;

import java.util.List;

/*
    负责问卷实体类Questionnaire及对应问题表questionnaire的操作
 */

public interface QuestionnaireDao extends JpaRepository<Questionnaire,Integer> {
    public Questionnaire findByQuestionnaireId(int questionnaireId);
    //更新问卷标题
    @Query("update Questionnaire set title = ?1 where questionnaireId = ?2")
    @Modifying
    public void updateTitle(String newTitle, int questionnaireId);
    //删除问卷
    @Query("delete from Questionnaire where questionnaireId = ?1")
    @Modifying
    public void dropQuestionnaire(int questionnaireId);
    //更新v1:通过项目Id读取旗下问卷信息
    public List<Questionnaire> findAllByProjectId(int projectId);
    //更新v1:通过项目id和问卷名读取问卷Id
    public Questionnaire findByProjectIdAndTitle(int projectId, String title);
}
