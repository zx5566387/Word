package word.dto;

import lombok.Data;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created By IntelliJ IDEA.
 * Author: AL-
 * Date: 2020/01/06 16:32
 */
@Data
public class LogSmallDTO implements Serializable {

    // 描述
    private String description;

    // 请求ip
    private String requestIp;

    // 请求耗时
    private Long time;

    private String address;

    private String browser;

    // 创建日期
    private Timestamp createTime;
}
