package word.mapper;

import word.dto.LogErrorDTO;
import word.domain.Log;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * Created By IntelliJ IDEA.
 * Author: AL-
 * Date: 2020/01/06 16:32
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LogErrorMapper extends BaseMapper<LogErrorDTO, Log> {

}