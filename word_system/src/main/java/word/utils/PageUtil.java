package word.utils;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By IntelliJ IDEA.
 * Author: AL-
 * Date: 2020/01/06 16:32
 * See: 分页工具
 */
public class PageUtil extends cn.hutool.core.util.PageUtil {

    /**
     * List 分页
     */
    public static List toPage(int page, int size , List list) {
        int fromIndex = page * size;
        int toIndex = page * size + size;

        if(fromIndex > list.size()){
            return new ArrayList();
        } else if(toIndex >= list.size()) {
            return list.subList(fromIndex,list.size());
        } else {
            return list.subList(fromIndex,toIndex);
        }
    }

    /**
     * Page 数据处理，预防redis反序列化报错
     */
    public static Map<String,Object> toPage(Page page) {
        Map<String,Object> map = new LinkedHashMap<>(2);
        map.put("content",page.getContent());
        map.put("totalElements",page.getTotalElements());
        return map;
    }

    /**
     * 自定义分页
     */
    public static Map<String,Object> toPageAndTableHeader(Page page,Object object) {
        Map<String,Object> map = new LinkedHashMap<>(2);
        // 数据
        map.put("content",page.getContent());
        // 分页数据 size limit total ...
        map.put("totalElements",page.getTotalElements());
        // 标题
        map.put("tableHeader",object);

        return map;
    }

    /**
     * 自定义分页 (带搜索默认值)
     */
    public static Map<String,Object> toPageAndTableHeaderAndSearch(Page page,Object object,Map<String,Object> maps) {
        Map<String,Object> map = new LinkedHashMap<>(2);
        // 数据
        map.put("content",page.getContent());
        // 分页数据 size limit total ...
        map.put("totalElements",page.getTotalElements());
        // 标题
        map.put("tableHeader",object);
        // 间接人工成本比例,非生产辅料成本比例
        map.put("labor",maps.get("labor"));
        map.put("excipients",maps.get("excipients"));

        return map;
    }

}
