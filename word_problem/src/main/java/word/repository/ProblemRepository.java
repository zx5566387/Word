package word.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import word.entity.Problem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by hp on 2020/2/5.
 */
public interface ProblemRepository extends JpaRepository<Problem, Long>, JpaSpecificationExecutor<Problem> {
    //查询具体的某一个问题
    Problem findByProblemId(int problemId);
    //查询某一个问卷下所有问题根据状态查询
    Page<Problem> findByQuestionnaireIdAndState(int questionnaireId,int state,Pageable pageable);

}
