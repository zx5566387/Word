package word.service.impl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import word.entity.Problem;
import word.repository.ProblemRepository;
import word.service.ProblemService;

/**
 * Created by hp on 2020/2/5.
 */
@Service
@CacheConfig(cacheNames = "problem")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ProblemServiceImpl implements ProblemService {
    private final ProblemRepository problemRepository;

    public ProblemServiceImpl(ProblemRepository problemRepository) {
        this.problemRepository = problemRepository;
    }

    @Override
    public void addProblem(Problem problem) {
        problem.setState(0);
        problemRepository.save(problem);
    }

    @Override
    public void updateProblem(Problem problem) {
        problemRepository.save(problem);
    }

    @Override
    public void delProblemByProblemId(Long problemId) {
        problemRepository.deleteById(problemId);
    }

    @Override
    public Problem findByProblemId(int problemId) {
        return problemRepository.findByProblemId(problemId);
    }

    @Override
    public Page<Problem> findByQuestionnaireIdAndState(int questionnaireId,int state, Pageable pageable) {
        return problemRepository.findByQuestionnaireIdAndState(questionnaireId,state,pageable);
    }
}
