package word.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import word.entity.Problem;


/**
 * Created by hp on 2020/2/5.
 */
public interface ProblemService {
    //添加问题
    void addProblem(Problem problem);
    //删除问题通过问题id
    void delProblemByProblemId(Long problemId);
    //修改问题
    void updateProblem(Problem problem);
    //查询具体的某一个问题
    Problem findByProblemId(int problemId);
    //查询某一个问卷下所有问题根据状态查询
    Page<Problem> findByQuestionnaireIdAndState(int questionnaireId, int state, Pageable pageable);
}
