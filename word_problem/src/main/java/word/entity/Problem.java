package word.entity;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by hp on 2020/2/5.
 */
@Entity
@Data
@Table(name="\"problem\"")
public class Problem  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer problemId;
    private Integer questionnaireId;
    private String serialNum;
    private String problemName;
    private String answer;
    private Integer state;
}
