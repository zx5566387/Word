package word.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import word.service.ProblemService;
import word.entity.Problem;


/**
 * Created by hp on 2020/2/5.
 */
@Slf4j
@RestController
@RequestMapping("/problem")
@Api(tags = "工具：问题模块")
public class ProblemController {

    private final ProblemService problemService;

    public ProblemController(ProblemService problemService) {
        this.problemService = problemService;
    }

    @ApiOperation("查询所有问题")
    @PostMapping("/getAll")
    public ResponseEntity getProblem(@RequestParam("questionnaireId")int questionnaireId, @RequestParam("state")int state, Pageable pageable){
        return new ResponseEntity<>(problemService.findByQuestionnaireIdAndState(questionnaireId,state,pageable), HttpStatus.OK);
    }

    @ApiOperation("创建问题")
    @PostMapping("/create")
    public ResponseEntity create(@RequestBody Problem problem){
        problemService.addProblem(problem);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation("修改问题")
    @PostMapping("/update")
    public ResponseEntity update(@RequestBody Problem problem){
        problemService.updateProblem(problem);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @ApiOperation("删除问题")
    @PostMapping("/del")
    public ResponseEntity delete(@RequestParam Long problemId){
        problemService.delProblemByProblemId(problemId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation("查询具体某个问题")
    @PostMapping("/findProblem")
    public ResponseEntity findProblem(@RequestParam int problemId){
        return new ResponseEntity(problemService.findByProblemId(problemId),HttpStatus.OK);
    }
}
