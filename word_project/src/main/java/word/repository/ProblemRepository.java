package word.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import word.dto.Project;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ProblemRepository extends JpaRepository<Project, Integer>, JpaSpecificationExecutor<Project> {

    @Query(value = "select p.projectId,p.projectName,p.projectDescription,p.createTime,p.state,p.userId from `user` t,project p where t.id = p.userId and p.userId = :userId ",nativeQuery = true)
    List<Project> queryByUserId(@Param("userId") Integer userId);


    @Modifying
    @Query(value = "update project set state = 1 where projectId = :projectId",nativeQuery = true)
    void deleteProblem(Integer projectId);

    @Modifying
    @Query(value = "update project set projectName = :projectName,projectDescription = :projectDescription  where projectId = :projectId and state = 0",nativeQuery = true)
    void updateProblem(Integer projectId,String projectName,String projectDescription);

    @Modifying
    @Query(value = "insert into project (userId,projectName,projectDescription,createTime) VALUES(?1,?2,?3,?4)",nativeQuery = true)
    void insertProblem(Integer projectId,String projectName,String projectDescription,String createTimt);
}
