package word.util;

/**
 * @author AL-
 * @see return constant
 */
public class Constant {
    // 成功
    public static final int SUCCESS_CODE = 100;

    // 成功
    public static final String SUCCESS_MSG = "Success";

    // 系统错误
    public static final int FAIL_CODE = 101;

    // 系统错误
    public static final String FAIL_MSG = "Failed";

    // 分配规则已存在
    public static final int FAIL_DRS = 102;

    // 分配规则已存在提示
    public static final String FAIL_DRS_MSG = "该规则已存在";

    // 型体不能为空
    public static final String FAIL_MOLDE = "型体不能为空";

    // 无操作权限
    public static final int CHECK_PERMISSION = 122;

}
