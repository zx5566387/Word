package word.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import java.io.Serializable;

public abstract class AbstractBean implements Serializable {

    private static final long serialVersionUID = 1L;

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
