package word.util;

public class AjaxResponse extends AbstractBean {

    private static final long serialVersionUID = 1L;

    private int status;

    private String message;

    private Object result;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public AjaxResponse() { }


    public static AjaxResponse reponseBody(int statusa, String message, Object result) {
    	AjaxResponse ajaxResponse = new AjaxResponse();
    	ajaxResponse.setStatus(statusa);
    	ajaxResponse.setMessage(message);
    	ajaxResponse.setResult(result);
    	return ajaxResponse;
    }
}
