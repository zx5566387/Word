package word.controller;

import lombok.extern.slf4j.Slf4j;
import word.dto.Project;
import word.service.ProjectService;
import word.util.AjaxResponse;
import word.util.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: drs_style
 * @author: HXM
 * @create: 2020-02-05
 **/
@Slf4j
@RestController
@RequestMapping("/project")
@Api(tags = "工具：项目模块")
public class ProjectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);


    @Autowired
    private ProjectService projectServiceImpl;


    @ApiOperation(value = "查询 所有项目模块")
    @PostMapping("/queryProblem")
    public AjaxResponse selectProblem(@RequestParam("userId")Integer userId){
        try {
            List<Project> problemList = projectServiceImpl.SelectProblem(userId);
            return AjaxResponse.reponseBody(Constant.SUCCESS_CODE, Constant.SUCCESS_MSG, problemList);
        }catch (Exception e){
            LOGGER.error("===> DrsController Query Method Error: ({})",e.getMessage());
            return AjaxResponse.reponseBody(Constant.FAIL_CODE, Constant.FAIL_MSG, null);
        }
    }

    @ApiOperation(value = "删除项目模块")
    @PostMapping("/deleteProblem")
    public AjaxResponse deleteProblem(@RequestParam("projectId")Integer projectId){
        try {
            projectServiceImpl.deleteProblem(projectId);
            return AjaxResponse.reponseBody(Constant.SUCCESS_CODE, Constant.SUCCESS_MSG, "");
        }catch (Exception e){
            LOGGER.error("===> DrsController Query Method Error: ({})",e.getMessage());
            return AjaxResponse.reponseBody(Constant.FAIL_CODE, Constant.FAIL_MSG, null);
        }
    }


    @ApiOperation(value = "修改项目模块")
    @PostMapping("/updateProblem")
    public AjaxResponse updateProblem(@RequestBody Project project){
        try {
            projectServiceImpl.updateProblem(project);
            return AjaxResponse.reponseBody(Constant.SUCCESS_CODE, Constant.SUCCESS_MSG, "");
        }catch (Exception e){
            LOGGER.error("===> DrsController Query Method Error: ({})",e.getMessage());
            return AjaxResponse.reponseBody(Constant.FAIL_CODE, Constant.FAIL_MSG, null);
        }
    }

    @ApiOperation(value = "新增项目模块")
    @PostMapping("/insertProblem")
    public AjaxResponse insertProblem(@RequestBody Project project){
        try {
            projectServiceImpl.insertProblem(project);
            return AjaxResponse.reponseBody(Constant.SUCCESS_CODE, Constant.SUCCESS_MSG, "");
        }catch (Exception e){
            LOGGER.error("===> DrsController Query Method Error: ({})",e.getMessage());
            return AjaxResponse.reponseBody(Constant.FAIL_CODE, Constant.FAIL_MSG, null);
        }
    }
}
