package word.service.impl;


import word.dto.Project;
import word.repository.ProblemRepository;
import word.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @program: drs_style
 * @author: HXM
 * @create: 2020-02-05
 **/

@Service(value = "projectServiceImpl")
public class ProjectServiceImpl implements ProjectService {


    @Autowired
    private ProblemRepository problemRepository;

    @Override
    public List<Project> SelectProblem(Integer userId) {
        return problemRepository.queryByUserId(userId);
    }

    @Override
    public void deleteProblem(Integer projectId) {
        problemRepository.deleteProblem(projectId);
    }

    @Override
    public void updateProblem(Project project) {
        Integer projectId = project.getProjectId();
        String projectName = project.getProjectName();
        String projectDescription = project.getProjectDescription();

        problemRepository.updateProblem(projectId,projectName,projectDescription);
    }

    @Override
    public void insertProblem(Project project) {
        Integer userId = project.getUserId();
        String projectName = project.getProjectName();
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createTimt = simpleDateFormat.format(date);;
        String projectDescription = project.getProjectDescription();
        problemRepository.insertProblem(userId,projectName,projectDescription,createTimt);
    }
}
