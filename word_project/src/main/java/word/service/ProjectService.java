package word.service;

import word.dto.Project;

import java.util.List;

/**
 * @program: drs_style
 * @author: HXM
 * @create: 2020-02-05
 **/
public interface ProjectService {
    List<Project> SelectProblem(Integer userId);

    void deleteProblem(Integer projectId);

    void updateProblem(Project projectId);

    void insertProblem(Project project);
}
