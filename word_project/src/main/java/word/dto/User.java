//package word.dto;
//
//import lombok.Data;
//import org.hibernate.annotations.CreationTimestamp;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Pattern;
//import java.sql.Timestamp;
//
///**
// * @program: drs_style
// * @author: HXM
// * @create: 2020-01-21
// **/
//@Entity
//@Data
//@Table(name = "\"user\"")
//public class User {
//
//    @Id
//    private Long id;
//
//    @NotBlank
//    @Column(unique = true)
//    private String username;
//
//    @NotBlank
//    private String email;
//
//    @NotBlank
//    private String phone;
//
//    @NotNull
//    private Boolean enabled;
//
//    private String password;
//
//    @Column(name = "create_time")
//    @CreationTimestamp
//    private Timestamp createTime;
//
//}
