package word.dto;

import lombok.Data;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @program: drs_style
 * @author: HXM
 * @create: 2020-02-05
 **/
@Entity
@Data
@Table(name = "\"project\"")
public class Project extends JpaRepositoriesAutoConfiguration {
    @Id
    @Column(name = "\"projectId\"")
    private Integer projectId;
    @Column(name = "\"userId\"")
    private Integer userId;
    @Column(name = "\"projectName\"")
    private String projectName;
    @Column(name = "\"projectDescription\"")
    private String projectDescription;
    @Column(name = "\"createTime\"")
    private Date createTime;
    @Column(name = "\"state\"")
    private Integer state;
}
