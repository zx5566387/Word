package word.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created By IntelliJ IDEA
 * Author 阿龙攻城狮
 * Date 2020/2/5 6:39 下午
 */
@RestController
@RequestMapping("/tool/replace")
@Api(tags = "工具：替换内容")
public class ReplacesController {

    @GetMapping
    @ApiOperation("替换Word内容")
    public void replaces(){
        System.err.println("1111");
    }

}
